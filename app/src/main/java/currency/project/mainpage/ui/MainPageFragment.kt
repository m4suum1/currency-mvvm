package currency.project.mainpage.ui

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import currency.project.R
import currency.project.common.mvvm.BaseFragment
import currency.project.databinding.FragmentMainPageBinding
import currency.project.mainpage.model.Currency
import currency.project.mainpage.ui.adapter.CurrencyType
import currency.project.mainpage.ui.adapter.MainPageAdapter
import currency.project.navigationpage.ui.NavigationPageFragment
import currency.project.utils.extensions.addScreen
import currency.project.utils.extensions.setAutoCompleteTextView
import currency.project.utils.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainPageFragment : BaseFragment(R.layout.fragment_main_page) {
    private val binding: FragmentMainPageBinding by viewBinding()
    private val viewModel: MainPageViewModel by viewModels()
    private val adapterItems: ArrayAdapter<String> by lazy {
        val currencies = resources.getStringArray(R.array.currencies)
        ArrayAdapter(this.requireContext(), R.layout.item_drop_down, currencies)
    }

    private val adapter: MainPageAdapter by lazy {
        MainPageAdapter { item ->
            println(item)
        }
    }

    override fun onResume() {
        super.onResume()
        with(binding.autoCompleteTextViewBaseCurrency) {
            setAutoCompleteTextView(adapterItems)
            setText(CurrencyType.USD.currency, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseCurrency = CurrencyType.USD.currency
        with(binding) {
            toolbarNavigation.setNavigationOnClickListener {
                addScreen(
                    NavigationPageFragment(),
                    enter = R.anim.nav_enter_nav,
                    exit = R.anim.nav_exit_nav
                )
            }
            val currencies = emptyList<String>()
            viewModel.getCurrencyData(baseCurrency, currencies)
            with(autoCompleteTextViewBaseCurrency) {
                setOnItemClickListener { parent, _, pos, _ ->
                    val newBaseCurrency = parent.getItemAtPosition(pos).toString()
                    viewModel.getCurrencyData(newBaseCurrency, currencies)
                }
            }
        }

        observe(viewModel.currencyStateFlow) { currencyData ->
            showData(currencyData.currencies)
        }

        observe(viewModel.loadingStateFlow){isLoading ->
            showLoading(isLoading = isLoading)
        }

        with(binding) {
            recyclerViewCurrencyList.layoutManager = LinearLayoutManager(context)
            recyclerViewCurrencyList.setHasFixedSize(true)
            recyclerViewCurrencyList.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewCurrencyList)
        }
    }
    private fun showData(data: List<Currency>) {
        adapter.setData(data)
    }

    private fun showLoading (isLoading: Boolean){
        binding.progressBar.isVisible = isLoading
    }
}
