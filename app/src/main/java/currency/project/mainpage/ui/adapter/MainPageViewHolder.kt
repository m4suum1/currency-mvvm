package currency.project.mainpage.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import currency.project.databinding.ItemCurrencyBinding
import currency.project.mainpage.model.Currency
import currency.project.utils.extensions.getDrawableIconFlag
import currency.project.utils.extensions.removeNulls
import currency.project.utils.extensions.removeNullsWithDot

class MainPageViewHolder (
    private val binding: ItemCurrencyBinding,
    private val clickOnItem: (Currency) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        onClickItem: (Currency) -> Unit
    ) : this(
        ItemCurrencyBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onClickItem
    )

    private lateinit var currencyType: String
    private lateinit var currencyValue: String
    private lateinit var item: Currency

    fun getData(item: Currency) {
        currencyType = item.currencyType
        currencyValue = item.currencyValue.removeNullsWithDot()
        this.item = item
    }

    fun onBind() {
        with(binding) {
            imageViewFlag.setImageResource(getDrawableIconFlag(currencyType))
            textViewCurrencyType.text = currencyType
            textViewCurrencyValue.text = currencyValue
        }
        itemView.setOnClickListener {
            clickOnItem(item)
        }
    }
}
