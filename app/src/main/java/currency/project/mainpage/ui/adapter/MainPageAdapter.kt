package currency.project.mainpage.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import currency.project.R
import currency.project.mainpage.model.Currency

class MainPageAdapter (
    private val clickOnItem:(Currency) -> Unit
    ) : RecyclerView.Adapter<MainPageViewHolder>() {

        private val data = mutableListOf<Currency>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainPageViewHolder {
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
            return MainPageViewHolder(parent, clickOnItem)
        }

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: MainPageViewHolder, position: Int) {
            val listItem = data[position]
            holder.getData(listItem)
            holder.onBind()
        }

        @SuppressLint("NotifyDataSetChanged")
        fun setData(items: List<Currency>) {
            data.clear()
            data.addAll(items)
            notifyDataSetChanged()
        }
    }
