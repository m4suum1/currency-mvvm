package currency.project.mainpage.ui

import currency.project.common.mvvm.BaseViewModel
import currency.project.mainpage.interactor.MainPageInteractor
import currency.project.mainpage.model.CurrencyData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainPageViewModel @Inject constructor(
    private val interactor: MainPageInteractor
) : BaseViewModel() {

    private val _currencyStateFlow = MutableStateFlow(CurrencyData(emptyList()))
    val currencyStateFlow = _currencyStateFlow.asStateFlow()

    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow = _loadingStateFlow.asStateFlow()

    fun getCurrencyData(baseCurrency: String, currencies: List<String>) {
        launch {
            try {
                _loadingStateFlow.tryEmit(true)
                var currencyData: CurrencyData
                withContext(Dispatchers.IO) {
                    currencyData = interactor.getCurrencyData(baseCurrency, currencies)
                }
                _currencyStateFlow.tryEmit(currencyData)
            } catch (c: CancellationException) {
                Timber.e("/*/ Error ${c.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }
}
