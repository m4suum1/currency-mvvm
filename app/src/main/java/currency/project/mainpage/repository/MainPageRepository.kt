package currency.project.mainpage.repository

import currency.project.mainpage.model.CurrencyData

interface MainPageRepository {
    suspend fun getCurrencyData(baseCurrency: String, currencies: List <String>): CurrencyData
    suspend fun getCurrencyFactor(baseCurrency: String, currencies: List <String>):Double
}
