package currency.project.mainpage.repository

import currency.project.mainpage.api.MainPageApi
import currency.project.mainpage.model.MainPageConverter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainPageRemoteRepository @Inject constructor(
    private val api: MainPageApi
) : MainPageRepository {
    override suspend fun getCurrencyData(
        baseCurrency: String, currencies: List<String>
    ) = withContext(Dispatchers.IO) {
        MainPageConverter.convert(
            api.getCurrencyData(
                baseCurrency = baseCurrency,
                currencies = currencies
            )
        )
    }

    override suspend fun getCurrencyFactor(
        baseCurrency: String, currencies: List<String>
    ) = withContext(Dispatchers.IO) {
        MainPageConverter.convertToFactor(
            api.getCurrencyData(
                baseCurrency = baseCurrency,
                currencies = currencies
            )
        )
    }
}
