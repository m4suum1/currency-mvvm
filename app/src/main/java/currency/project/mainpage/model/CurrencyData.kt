package currency.project.mainpage.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CurrencyData(
    val currencies: List<Currency>
):Parcelable
