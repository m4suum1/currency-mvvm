package currency.project.mainpage.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Currency(
    val currencyType: String,
    val currencyValue: String
):Parcelable
