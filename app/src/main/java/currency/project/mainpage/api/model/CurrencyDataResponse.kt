package currency.project.mainpage.api.model

import com.google.gson.annotations.SerializedName

data class CurrencyDataResponse(
    @SerializedName("data")
    val currency: CurrenciesResponse
)
