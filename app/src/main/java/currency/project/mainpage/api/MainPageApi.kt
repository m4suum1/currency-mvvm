package currency.project.mainpage.api

import currency.project.mainpage.api.model.CurrencyDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MainPageApi {
    @GET ("v1/latest")
    suspend fun getCurrencyData (
        @Query("apikey") apiKey: String = "NJLJLhV069pNSDirgFCzdj9XYqdquk85XQ1dKNWY",
        @Query ("base_currency") baseCurrency: String,
        @Query ("currencies") currencies: List <String>
    ): CurrencyDataResponse
}