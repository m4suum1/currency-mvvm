package currency.project.mainpage.di

import currency.project.mainpage.api.MainPageApi
import currency.project.mainpage.interactor.MainPageInteractor
import currency.project.mainpage.repository.MainPageRemoteRepository
import currency.project.mainpage.repository.MainPageRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class MainPageModule {
    @Binds
    @Singleton
    abstract fun bindRepository(
        repository: MainPageRemoteRepository
    ): MainPageRepository

    companion object {
        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): MainPageApi = retrofit.create(MainPageApi::class.java)

        @Provides
        @Singleton
        fun provideInteractor(repository: MainPageRepository): MainPageInteractor =
            MainPageInteractor(repository)
    }
}
