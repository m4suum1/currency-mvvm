package currency.project.mainpage.interactor

import currency.project.mainpage.repository.MainPageRepository
import javax.inject.Inject

class MainPageInteractor @Inject constructor(
    private val repository: MainPageRepository
){
    suspend fun getCurrencyData(baseCurrency: String, currencies: List<String>) =
        repository.getCurrencyData(baseCurrency, currencies)

    suspend fun getCurrencyFactor(baseCurrency: String, currencies: List<String>) =
        repository.getCurrencyFactor(baseCurrency, currencies)
}
