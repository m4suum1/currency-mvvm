package currency.project.navigationpage.ui

import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import currency.project.R
import currency.project.common.mvvm.BaseFragment
import currency.project.convertpage.ui.ConvertPageFragment
import currency.project.databinding.FragmentNavigationBinding
import currency.project.signinpage.ui.SignInPageFragment
import currency.project.utils.extensions.popScreen
import currency.project.utils.extensions.replaceScreen
import currency.project.utils.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NavigationPageFragment : BaseFragment(R.layout.fragment_navigation) {
    private val binding: FragmentNavigationBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with (binding){
            buttonConvertCurrency.setOnClickListener {
                replaceScreen(ConvertPageFragment(), popCurrent = true)
            }
            buttonLogout.setOnClickListener {
                FirebaseAuth.getInstance().signOut()
                replaceScreen(SignInPageFragment(), clearBackStack = true)
            }
            toolbarNavigation.setNavigationOnClickListener {
                popScreen()
            }
        }
    }
}
