package currency.project.utils.extensions

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.behavior.SwipeDismissBehavior
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import currency.project.R

fun Activity.hideKeyboard() {
    currentFocus?.hideKeyboard()
}

fun Fragment.hideKeyboardDrop() {
    requireActivity().hideKeyboard()
}

fun View.hideKeyboard() {
    post {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(windowToken, 0)
    }
}

fun Fragment.createSnackBar(view: View, message: String) {
    val behavior = BaseTransientBottomBar.Behavior().apply {
        setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY)
    }
    context?.let {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .setActionTextColor(ContextCompat.getColor(it, R.color.lotus))
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).setBehavior(behavior)
            .setBackgroundTint(ContextCompat.getColor(it, R.color.snack_bar_background))
            .show()
    }
}

fun FragmentActivity.createSnackBar(view: View, message: String) {
    val behavior = BaseTransientBottomBar.Behavior().apply {
        setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY)
    }
    this.baseContext?.let {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .setActionTextColor(ContextCompat.getColor(it, R.color.lotus))
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).setBehavior(behavior)
            .setBackgroundTint(ContextCompat.getColor(it, R.color.grey))
            .show()
    }
}
