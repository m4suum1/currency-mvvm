package currency.project.utils.extensions

import android.text.SpannableString
import android.text.style.UnderlineSpan
import currency.project.R
import currency.project.mainpage.ui.adapter.CurrencyType

fun makeRatio(firstCurrency: String, secondCurrency: String, currencyFactor: Double) = buildString {
    append("1")
        .append(" ")
        .append(firstCurrency)
        .append(" = ")
        .append(currencyFactor.toString().removeNullsWithDot())
        .append(" ")
        .append(secondCurrency)
}


fun makeCurrencyName(name: String?): SpannableString {
    val secondCurrencyName = name.toString()
    val underlinedCurrencyName = SpannableString(secondCurrencyName)
    underlinedCurrencyName.setSpan(UnderlineSpan(), 0, secondCurrencyName.length, 0)
    return underlinedCurrencyName
}

fun makeCurrencyValue(resultAmount: Double, symbol: String?) = buildString {
    append(resultAmount.formatDouble().removeNullsWithDot())
        .append(" ")
        .append(symbol)
}

fun getDrawableIconFlag(type: String): Int =
    when (type) {
        CurrencyType.EUR.currency -> R.drawable.ic_euro_union_icon
        CurrencyType.USD.currency -> R.drawable.ic_united_states_icon
        CurrencyType.JPY.currency -> R.drawable.ic_japan_icon
        CurrencyType.BGN.currency -> R.drawable.ic_bulgaria_icon
        CurrencyType.CZK.currency -> R.drawable.ic_czech_republic_icon
        CurrencyType.DKK.currency -> R.drawable.ic_denmark_icon
        CurrencyType.GBP.currency -> R.drawable.ic_united_kingdom_icon
        CurrencyType.HUF.currency -> R.drawable.ic_hungary_icon
        CurrencyType.PLN.currency -> R.drawable.ic_poland_icon
        CurrencyType.RON.currency -> R.drawable.ic_romania_icon
        CurrencyType.SEK.currency -> R.drawable.ic_sweden_icon
        CurrencyType.CHF.currency -> R.drawable.ic_switzerland_icon
        CurrencyType.ISK.currency -> R.drawable.ic_iceland_icon
        CurrencyType.NOK.currency -> R.drawable.ic_norway_icon
        CurrencyType.HRK.currency -> R.drawable.ic_croatia_icon
        CurrencyType.RUB.currency -> R.drawable.ic_russia_icon
        CurrencyType.TRY.currency -> R.drawable.ic_turkey_icon
        CurrencyType.AUD.currency -> R.drawable.ic_australia_icon
        CurrencyType.BRL.currency -> R.drawable.ic_brazil_icon
        CurrencyType.CAD.currency -> R.drawable.ic_canada_icon
        CurrencyType.CNY.currency -> R.drawable.ic_china_icon
        CurrencyType.HKD.currency -> R.drawable.ic_hong_kong_icon
        CurrencyType.IDR.currency -> R.drawable.ic_indonesia_icon
        CurrencyType.ILS.currency -> R.drawable.ic_israel_icon
        CurrencyType.INR.currency -> R.drawable.ic_india_icon
        CurrencyType.KRW.currency -> R.drawable.ic_south_korea_icon
        CurrencyType.MXN.currency -> R.drawable.ic_mexico_icon
        CurrencyType.MYR.currency -> R.drawable.ic_malaysia_icon
        CurrencyType.NZD.currency -> R.drawable.ic_new_zealand_icon
        CurrencyType.PHP.currency -> R.drawable.ic_philippines_icon
        CurrencyType.SGD.currency -> R.drawable.ic_singapore_icon
        CurrencyType.THB.currency -> R.drawable.ic_thailand_icon
        CurrencyType.ZAR.currency -> R.drawable.ic_south_africa_icon
        else -> throw IllegalStateException()
    }
