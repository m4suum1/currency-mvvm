package currency.project.utils.extensions

import android.widget.ArrayAdapter
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputLayout
import currency.project.R

private const val PASSWORD_MAX_SIZE = 20
private const val PASSWORD_MIN_SIZE = 6
private const val EMAIL_MAX_SIZE = 24
private const val EMAIL_MIN_SIZE = 9

fun TextInputLayout.catchPasswordError(text: String) {
    if (text.length in PASSWORD_MIN_SIZE..PASSWORD_MAX_SIZE) {
        isErrorEnabled = false
    } else if (text.length < PASSWORD_MIN_SIZE) {
        error =
            resources.getString(R.string.text_input_layout_password_error_min)
        isErrorEnabled = true
    } else {
        error =
            resources.getString(R.string.text_input_layout_password_error_max)
        isErrorEnabled = true
    }
}

fun TextInputLayout.catchEmailError(text: String) {
    if (text.length in EMAIL_MIN_SIZE..EMAIL_MAX_SIZE) {
        isErrorEnabled = false
    } else if (text.length < EMAIL_MIN_SIZE) {
        error =
            resources.getString(R.string.text_input_layout_email_error_min)
        isErrorEnabled = true
    } else {
        error =
            resources.getString(R.string.text_input_layout_email_error_max)
        isErrorEnabled = true
    }
}


private const val TEXT_MAX_SIZE = 20
private const val TEXT_MIN_SIZE = 1

fun TextInputLayout.catchMoneyAmountError(text: String) {
    if (text.length in TEXT_MIN_SIZE..TEXT_MAX_SIZE) {
        isErrorEnabled = false
    } else if (text.length < TEXT_MIN_SIZE) {
        error =
            resources.getString(R.string.text_input_layout_currency_error_min)
        isErrorEnabled = true
    } else {
        error =
            resources.getString(R.string.text_input_layout_currency_error_max)
        isErrorEnabled = true
    }
}

fun MaterialAutoCompleteTextView.setAutoCompleteTextView(adapterItems: ArrayAdapter<String>) {
    setAdapter(adapterItems)
    setDropDownBackgroundDrawable(
        ResourcesCompat.getDrawable(
            resources,
            R.drawable.drop_down_item_background,
            null
        )
    )
}
