package currency.project.utils

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult


object FirebaseException : Exception() {
    fun getErrorCode(task: Task<AuthResult>): String {
        return task.exception?.message.toString()
    }
}