package currency.project.registerpage.ui

import android.os.Bundle
import android.view.View
import androidx.core.widget.doAfterTextChanged
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import currency.project.R
import currency.project.common.mvvm.BaseFragment
import currency.project.databinding.FragmentRegisterPageBinding
import currency.project.mainpage.ui.MainPageFragment
import currency.project.utils.FirebaseException
import currency.project.utils.extensions.*
import currency.project.utils.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class RegisterPageFragment : BaseFragment(R.layout.fragment_register_page) {
    private val binding: FragmentRegisterPageBinding by viewBinding()
    private val auth: FirebaseAuth by lazy {
        Firebase.auth
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            textInputEditTextPassword.doAfterTextChanged {
                textInputLayoutPassword.catchPasswordError(it.toString())
            }
            textInputEditTextConfirmPassword.doAfterTextChanged {
                textInputLayoutConfirmPassword.catchPasswordError(it.toString())
            }
            textInputEditTextLogin.doAfterTextChanged {
                textInputLayoutLogin.catchEmailError(it.toString())
            }
            buttonRegister.setOnClickListener {
                hideKeyboardDrop()
                val email = textInputEditTextLogin.text.toString()
                val password = textInputEditTextPassword.text.toString()
                val confirmPassword = textInputEditTextConfirmPassword.text.toString()
                if (
                    email.isEmpty() ||
                    password.isEmpty() ||
                    confirmPassword.isEmpty()
                ) createSnackBar(
                    myCoordinatorLayout,
                    resources.getString(R.string.all_fields_snack)
                )
                else if (email.isNotEmpty() && password.isNotEmpty()) {
                    if (password == confirmPassword) {
                        launch {
                            signUp(auth, email, password, myCoordinatorLayout)
                        }
                    } else createSnackBar(
                        myCoordinatorLayout,
                        resources.getString(R.string.confirm_passwords_snack)
                    )
                }
            }
            buttonSignIn.setOnClickListener {
                popScreen()
            }
        }
    }

    private suspend fun signUp(
        auth: FirebaseAuth,
        email: String,
        password: String,
        myCoordinatorLayout: View
    ) {
        var authorization: Task<AuthResult>
        withContext(Dispatchers.IO) {
            authorization = auth.createUserWithEmailAndPassword(email, password)
        }
        authorization.addOnCompleteListener { task ->
            if (task.isSuccessful) replaceScreen(
                MainPageFragment(),
                clearBackStack = true
            )
            else createSnackBar(
                myCoordinatorLayout,
                FirebaseException.getErrorCode(task)
            )
        }
    }
}
