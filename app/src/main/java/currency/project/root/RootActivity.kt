package currency.project.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import currency.project.R
import currency.project.databinding.ActivityRootBinding
import currency.project.signinpage.ui.SignInPageFragment
import currency.project.utils.extensions.popFeature
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRootBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(SignInPageFragment())
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("popFeature()", "currency.project.utils.extensions.popFeature")
    )
    override fun onBackPressed() {
        popFeature()
    }

    private fun replace(
        fragment: Fragment,
        tag: String = fragment::class.java.name
    ) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }
}
