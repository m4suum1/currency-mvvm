package currency.project.convertpage.repository

import currency.project.convertpage.api.ConvertPageApi
import currency.project.convertpage.model.ConvertPageConverter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ConvertPageRemoteRepository @Inject constructor(
    private val api: ConvertPageApi
) : ConvertPageRepository {
    override suspend fun getCurrencyDescription(
        currencies: List<String>
    ) = withContext(Dispatchers.IO) {
        ConvertPageConverter.convert(
            api.getCurrencyDescription(
                currencies = currencies
            )
        )
    }
}
