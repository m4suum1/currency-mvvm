package currency.project.convertpage.repository

import currency.project.convertpage.model.CurrencyDescription

interface ConvertPageRepository {
    suspend fun getCurrencyDescription(currencies: List<String>): CurrencyDescription
}
