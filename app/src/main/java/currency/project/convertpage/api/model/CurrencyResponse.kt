package currency.project.convertpage.api.model

import com.google.gson.annotations.SerializedName

data class CurrencyResponse(
    @SerializedName("EUR")
    val eur: CertainCurrencyResponse?,
    @SerializedName("USD")
    val usd: CertainCurrencyResponse?,
    @SerializedName("JPY")
    val jpy: CertainCurrencyResponse?,
    @SerializedName("BGN")
    val bgn: CertainCurrencyResponse?,
    @SerializedName("CZK")
    val czk: CertainCurrencyResponse?,
    @SerializedName("DKK")
    val dkk: CertainCurrencyResponse?,
    @SerializedName("GBP")
    val gbp: CertainCurrencyResponse?,
    @SerializedName("HUF")
    val huf: CertainCurrencyResponse?,
    @SerializedName("PLN")
    val pln: CertainCurrencyResponse?,
    @SerializedName("RON")
    val ron: CertainCurrencyResponse?,
    @SerializedName("SEK")
    val sek: CertainCurrencyResponse?,
    @SerializedName("CHF")
    val chf: CertainCurrencyResponse?,
    @SerializedName("ISK")
    val isk: CertainCurrencyResponse?,
    @SerializedName("NOK")
    val nok: CertainCurrencyResponse?,
    @SerializedName("HRK")
    val hrk: CertainCurrencyResponse?,
    @SerializedName("RUB")
    val rub: CertainCurrencyResponse?,
    @SerializedName("TRY")
    val trY: CertainCurrencyResponse?,
    @SerializedName("AUD")
    val aud: CertainCurrencyResponse?,
    @SerializedName("BRL")
    val brl: CertainCurrencyResponse?,
    @SerializedName("CAD")
    val cad: CertainCurrencyResponse?,
    @SerializedName("CNY")
    val cny: CertainCurrencyResponse?,
    @SerializedName("HKD")
    val hkd: CertainCurrencyResponse?,
    @SerializedName("IDR")
    val idr: CertainCurrencyResponse?,
    @SerializedName("ILS")
    val ils: CertainCurrencyResponse?,
    @SerializedName("INR")
    val inr: CertainCurrencyResponse?,
    @SerializedName("KRW")
    val krw: CertainCurrencyResponse?,
    @SerializedName("MXN")
    val mxn: CertainCurrencyResponse?,
    @SerializedName("MYR")
    val myr: CertainCurrencyResponse?,
    @SerializedName("NZD")
    val nzd: CertainCurrencyResponse?,
    @SerializedName("PHP")
    val php: CertainCurrencyResponse?,
    @SerializedName("SGD")
    val sgd: CertainCurrencyResponse?,
    @SerializedName("THB")
    val thb: CertainCurrencyResponse?,
    @SerializedName("ZAR")
    val zar: CertainCurrencyResponse?
)
