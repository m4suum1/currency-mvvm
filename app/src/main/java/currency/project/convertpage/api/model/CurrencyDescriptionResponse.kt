package currency.project.convertpage.api.model

import com.google.gson.annotations.SerializedName

data class CurrencyDescriptionResponse(
    @SerializedName("data")
    val currency: CurrencyResponse
)
