package currency.project.convertpage.api

import currency.project.convertpage.api.model.CurrencyDescriptionResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ConvertPageApi {
    @GET("v1/currencies")
    suspend fun getCurrencyDescription(
        @Query("apikey") apiKey: String = "NJLJLhV069pNSDirgFCzdj9XYqdquk85XQ1dKNWY",
        @Query("currencies") currencies: List<String>
    ): CurrencyDescriptionResponse
}
