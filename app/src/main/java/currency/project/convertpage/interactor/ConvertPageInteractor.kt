package currency.project.convertpage.interactor

import currency.project.convertpage.repository.ConvertPageRepository
import javax.inject.Inject

class ConvertPageInteractor @Inject constructor(
    private val repository: ConvertPageRepository
) {
    suspend fun getCurrencyDescription(currencies: List<String>) = repository.getCurrencyDescription(currencies = currencies)
}
