package currency.project.convertpage.model

import currency.project.convertpage.api.model.CurrencyDescriptionResponse

object ConvertPageConverter {
    fun convert(data: CurrencyDescriptionResponse): CurrencyDescription {

        val currencies: List<CurrencyDescription> by lazy {
            listOf(
                CurrencyDescription(
                    symbol = data.currency.eur?.symbol,
                    name = data.currency.eur?.name,
                    symbolNative = data.currency.eur?.symbolNative,
                    decimalDigits = data.currency.eur?.decimalDigits,
                    rounding = data.currency.eur?.rounding,
                    code = data.currency.eur?.code,
                    namePlural = data.currency.eur?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.usd?.symbol,
                    name = data.currency.usd?.name,
                    symbolNative = data.currency.usd?.symbolNative,
                    decimalDigits = data.currency.usd?.decimalDigits,
                    rounding = data.currency.usd?.rounding,
                    code = data.currency.usd?.code,
                    namePlural = data.currency.usd?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.jpy?.symbol,
                    name = data.currency.jpy?.name,
                    symbolNative = data.currency.jpy?.symbolNative,
                    decimalDigits = data.currency.jpy?.decimalDigits,
                    rounding = data.currency.jpy?.rounding,
                    code = data.currency.jpy?.code,
                    namePlural = data.currency.jpy?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.bgn?.symbol,
                    name = data.currency.bgn?.name,
                    symbolNative = data.currency.bgn?.symbolNative,
                    decimalDigits = data.currency.bgn?.decimalDigits,
                    rounding = data.currency.bgn?.rounding,
                    code = data.currency.bgn?.code,
                    namePlural = data.currency.bgn?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.czk?.symbol,
                    name = data.currency.czk?.name,
                    symbolNative = data.currency.czk?.symbolNative,
                    decimalDigits = data.currency.czk?.decimalDigits,
                    rounding = data.currency.czk?.rounding,
                    code = data.currency.czk?.code,
                    namePlural = data.currency.czk?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.dkk?.symbol,
                    name = data.currency.dkk?.name,
                    symbolNative = data.currency.dkk?.symbolNative,
                    decimalDigits = data.currency.dkk?.decimalDigits,
                    rounding = data.currency.dkk?.rounding,
                    code = data.currency.dkk?.code,
                    namePlural = data.currency.dkk?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.gbp?.symbol,
                    name = data.currency.gbp?.name,
                    symbolNative = data.currency.gbp?.symbolNative,
                    decimalDigits = data.currency.gbp?.decimalDigits,
                    rounding = data.currency.gbp?.rounding,
                    code = data.currency.gbp?.code,
                    namePlural = data.currency.gbp?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.huf?.symbol,
                    name = data.currency.huf?.name,
                    symbolNative = data.currency.huf?.symbolNative,
                    decimalDigits = data.currency.huf?.decimalDigits,
                    rounding = data.currency.huf?.rounding,
                    code = data.currency.huf?.code,
                    namePlural = data.currency.huf?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.pln?.symbol,
                    name = data.currency.pln?.name,
                    symbolNative = data.currency.pln?.symbolNative,
                    decimalDigits = data.currency.pln?.decimalDigits,
                    rounding = data.currency.pln?.rounding,
                    code = data.currency.pln?.code,
                    namePlural = data.currency.pln?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.ron?.symbol,
                    name = data.currency.ron?.name,
                    symbolNative = data.currency.ron?.symbolNative,
                    decimalDigits = data.currency.ron?.decimalDigits,
                    rounding = data.currency.ron?.rounding,
                    code = data.currency.ron?.code,
                    namePlural = data.currency.ron?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.sek?.symbol,
                    name = data.currency.sek?.name,
                    symbolNative = data.currency.sek?.symbolNative,
                    decimalDigits = data.currency.sek?.decimalDigits,
                    rounding = data.currency.sek?.rounding,
                    code = data.currency.sek?.code,
                    namePlural = data.currency.sek?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.chf?.symbol,
                    name = data.currency.chf?.name,
                    symbolNative = data.currency.chf?.symbolNative,
                    decimalDigits = data.currency.chf?.decimalDigits,
                    rounding = data.currency.chf?.rounding,
                    code = data.currency.chf?.code,
                    namePlural = data.currency.chf?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.isk?.symbol,
                    name = data.currency.isk?.name,
                    symbolNative = data.currency.isk?.symbolNative,
                    decimalDigits = data.currency.isk?.decimalDigits,
                    rounding = data.currency.isk?.rounding,
                    code = data.currency.isk?.code,
                    namePlural = data.currency.isk?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.nok?.symbol,
                    name = data.currency.nok?.name,
                    symbolNative = data.currency.nok?.symbolNative,
                    decimalDigits = data.currency.nok?.decimalDigits,
                    rounding = data.currency.nok?.rounding,
                    code = data.currency.nok?.code,
                    namePlural = data.currency.nok?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.hrk?.symbol,
                    name = data.currency.hrk?.name,
                    symbolNative = data.currency.hrk?.symbolNative,
                    decimalDigits = data.currency.hrk?.decimalDigits,
                    rounding = data.currency.hrk?.rounding,
                    code = data.currency.hrk?.code,
                    namePlural = data.currency.hrk?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.rub?.symbol,
                    name = data.currency.rub?.name,
                    symbolNative = data.currency.rub?.symbolNative,
                    decimalDigits = data.currency.rub?.decimalDigits,
                    rounding = data.currency.rub?.rounding,
                    code = data.currency.rub?.code,
                    namePlural = data.currency.rub?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.trY?.symbol,
                    name = data.currency.trY?.name,
                    symbolNative = data.currency.trY?.symbolNative,
                    decimalDigits = data.currency.trY?.decimalDigits,
                    rounding = data.currency.trY?.rounding,
                    code = data.currency.trY?.code,
                    namePlural = data.currency.trY?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.aud?.symbol,
                    name = data.currency.aud?.name,
                    symbolNative = data.currency.aud?.symbolNative,
                    decimalDigits = data.currency.aud?.decimalDigits,
                    rounding = data.currency.aud?.rounding,
                    code = data.currency.aud?.code,
                    namePlural = data.currency.aud?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.brl?.symbol,
                    name = data.currency.brl?.name,
                    symbolNative = data.currency.brl?.symbolNative,
                    decimalDigits = data.currency.brl?.decimalDigits,
                    rounding = data.currency.brl?.rounding,
                    code = data.currency.brl?.code,
                    namePlural = data.currency.brl?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.cad?.symbol,
                    name = data.currency.cad?.name,
                    symbolNative = data.currency.cad?.symbolNative,
                    decimalDigits = data.currency.cad?.decimalDigits,
                    rounding = data.currency.cad?.rounding,
                    code = data.currency.cad?.code,
                    namePlural = data.currency.cad?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.cny?.symbol,
                    name = data.currency.cny?.name,
                    symbolNative = data.currency.cny?.symbolNative,
                    decimalDigits = data.currency.cny?.decimalDigits,
                    rounding = data.currency.cny?.rounding,
                    code = data.currency.cny?.code,
                    namePlural = data.currency.cny?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.hkd?.symbol,
                    name = data.currency.hkd?.name,
                    symbolNative = data.currency.hkd?.symbolNative,
                    decimalDigits = data.currency.hkd?.decimalDigits,
                    rounding = data.currency.hkd?.rounding,
                    code = data.currency.hkd?.code,
                    namePlural = data.currency.hkd?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.idr?.symbol,
                    name = data.currency.idr?.name,
                    symbolNative = data.currency.idr?.symbolNative,
                    decimalDigits = data.currency.idr?.decimalDigits,
                    rounding = data.currency.idr?.rounding,
                    code = data.currency.idr?.code,
                    namePlural = data.currency.idr?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.ils?.symbol,
                    name = data.currency.ils?.name,
                    symbolNative = data.currency.ils?.symbolNative,
                    decimalDigits = data.currency.ils?.decimalDigits,
                    rounding = data.currency.ils?.rounding,
                    code = data.currency.ils?.code,
                    namePlural = data.currency.ils?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.inr?.symbol,
                    name = data.currency.inr?.name,
                    symbolNative = data.currency.inr?.symbolNative,
                    decimalDigits = data.currency.inr?.decimalDigits,
                    rounding = data.currency.inr?.rounding,
                    code = data.currency.inr?.code,
                    namePlural = data.currency.inr?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.krw?.symbol,
                    name = data.currency.krw?.name,
                    symbolNative = data.currency.krw?.symbolNative,
                    decimalDigits = data.currency.krw?.decimalDigits,
                    rounding = data.currency.krw?.rounding,
                    code = data.currency.krw?.code,
                    namePlural = data.currency.krw?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.mxn?.symbol,
                    name = data.currency.mxn?.name,
                    symbolNative = data.currency.mxn?.symbolNative,
                    decimalDigits = data.currency.mxn?.decimalDigits,
                    rounding = data.currency.mxn?.rounding,
                    code = data.currency.mxn?.code,
                    namePlural = data.currency.mxn?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.myr?.symbol,
                    name = data.currency.myr?.name,
                    symbolNative = data.currency.myr?.symbolNative,
                    decimalDigits = data.currency.myr?.decimalDigits,
                    rounding = data.currency.myr?.rounding,
                    code = data.currency.myr?.code,
                    namePlural = data.currency.myr?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.nzd?.symbol,
                    name = data.currency.nzd?.name,
                    symbolNative = data.currency.nzd?.symbolNative,
                    decimalDigits = data.currency.nzd?.decimalDigits,
                    rounding = data.currency.nzd?.rounding,
                    code = data.currency.nzd?.code,
                    namePlural = data.currency.nzd?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.php?.symbol,
                    name = data.currency.php?.name,
                    symbolNative = data.currency.php?.symbolNative,
                    decimalDigits = data.currency.php?.decimalDigits,
                    rounding = data.currency.php?.rounding,
                    code = data.currency.php?.code,
                    namePlural = data.currency.php?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.sgd?.symbol,
                    name = data.currency.sgd?.name,
                    symbolNative = data.currency.sgd?.symbolNative,
                    decimalDigits = data.currency.sgd?.decimalDigits,
                    rounding = data.currency.sgd?.rounding,
                    code = data.currency.sgd?.code,
                    namePlural = data.currency.sgd?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.thb?.symbol,
                    name = data.currency.thb?.name,
                    symbolNative = data.currency.thb?.symbolNative,
                    decimalDigits = data.currency.thb?.decimalDigits,
                    rounding = data.currency.thb?.rounding,
                    code = data.currency.thb?.code,
                    namePlural = data.currency.thb?.namePlural
                ),
                CurrencyDescription(
                    symbol = data.currency.zar?.symbol,
                    name = data.currency.zar?.name,
                    symbolNative = data.currency.zar?.symbolNative,
                    decimalDigits = data.currency.zar?.decimalDigits,
                    rounding = data.currency.zar?.rounding,
                    code = data.currency.zar?.code,
                    namePlural = data.currency.zar?.namePlural
                )
            )
        }
        return currencies.filter {
            it.name != null
        }[0]
    }
}
