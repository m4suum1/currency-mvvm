package currency.project.convertpage.di

import currency.project.convertpage.api.ConvertPageApi
import currency.project.convertpage.interactor.ConvertPageInteractor
import currency.project.convertpage.repository.ConvertPageRemoteRepository
import currency.project.convertpage.repository.ConvertPageRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ConvertPageModule {

    @Binds
    @Singleton
    abstract fun bindConvertRepository(
        repository: ConvertPageRemoteRepository
    ): ConvertPageRepository

    companion object {
        @Provides
        @Singleton
        fun provideConvertApi(retrofit: Retrofit): ConvertPageApi =
            retrofit.create(ConvertPageApi::class.java)

        @Provides
        @Singleton
        fun provideConvertInteractor(repository: ConvertPageRepository): ConvertPageInteractor =
            ConvertPageInteractor(repository)
    }
}
