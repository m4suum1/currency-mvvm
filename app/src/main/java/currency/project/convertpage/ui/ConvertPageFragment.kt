package currency.project.convertpage.ui

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import currency.project.R
import currency.project.common.mvvm.BaseFragment
import currency.project.databinding.FragmentConvertPageBinding
import currency.project.utils.extensions.*
import currency.project.utils.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ConvertPageFragment : BaseFragment(R.layout.fragment_convert_page) {
    private val binding: FragmentConvertPageBinding by viewBinding()
    private val viewModel: ConvertPageViewModel by viewModels()
    private val adapterItems: ArrayAdapter<String> by lazy {
        val currencies = resources.getStringArray(R.array.currencies)
        ArrayAdapter(this.requireContext(), R.layout.item_drop_down, currencies)
    }

    override fun onResume() {
        super.onResume()

        with(binding) {
            autoCompleteTextFirstViewCurrency.setAutoCompleteTextView(adapterItems)
            autoCompleteTextViewSecondCurrency.setAutoCompleteTextView(adapterItems)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var resultAmount = 0.0
        var givenAmount = 0.0
        lateinit var firstCurrency: String
        lateinit var secondCurrency: String

        with(binding) {
            with(autoCompleteTextFirstViewCurrency) {
                setOnClickListener { hideKeyboardDrop() }
                setOnItemClickListener { parent, _, pos, _ ->
                    firstCurrency = parent.getItemAtPosition(pos).toString()
                    hideKeyboardDrop()
                }
            }
            with(autoCompleteTextViewSecondCurrency) {
                setOnClickListener { hideKeyboardDrop() }
                setOnItemClickListener { parent, _, pos, _ ->
                    secondCurrency = parent.getItemAtPosition(pos).toString()
                    hideKeyboardDrop()
                }
            }
        }

        with(binding) {
            textInputEditTextEnterAmount.doAfterTextChanged {
                textInputLayoutEnterAmount.catchMoneyAmountError(it.toString())
            }
            buttonConvert.setOnClickListener {
                hideKeyboardDrop()
                try {
                    givenAmount = binding.textInputEditTextEnterAmount.text.toString().toDouble()
                    startConversion(firstCurrency, secondCurrency)
                } catch (t: Throwable) {
                    createSnackBar(
                        binding.myCoordinatorLayout,
                        getString(R.string.all_fields_snack)
                    )
                }
            }
            toolbarNavigation.setNavigationOnClickListener {
                popScreen()
            }
        }
        observe(viewModel.currencyFactorStateFlow) { currencyFactor ->
            if (currencyFactor != 0.0) {
                resultAmount = (givenAmount * currencyFactor)
                binding.textViewRatioValue.text =
                    makeRatio(firstCurrency, secondCurrency, currencyFactor)
            }
        }
        observe(viewModel.currencyDescriptionStateFlow) { currencyDescription ->
            with(binding) {
                if (currencyDescription.name != null) {
                    textViewCurrency.text = makeCurrencyName(currencyDescription.name)
                    textViewCurrencyValue.text =
                        makeCurrencyValue(resultAmount, currencyDescription.symbolNative)
                    showInfo(true)
                }
            }
        }
        observe(viewModel.loadingStateFlow) { isLoading ->
            showLoading(isLoading = isLoading)
        }
    }

    private fun startConversion(firstCurrency: String, secondCurrency: String) {
        val currencies = listOf(secondCurrency)
        viewModel.getCurrencyFactor(baseCurrency = firstCurrency, currencies)
        viewModel.getCurrencyDescription(currencies)
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.isVisible = isLoading
    }

    private fun showInfo(hasInfo: Boolean) {
        binding.constraintLayoutInfo.isVisible = hasInfo
    }
}
