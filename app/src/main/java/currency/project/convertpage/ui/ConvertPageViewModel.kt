package currency.project.convertpage.ui

import currency.project.common.mvvm.BaseViewModel
import currency.project.convertpage.interactor.ConvertPageInteractor
import currency.project.convertpage.model.CurrencyDescription
import currency.project.mainpage.interactor.MainPageInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ConvertPageViewModel @Inject constructor(
    private val interactorData: MainPageInteractor,
    private val interactorDescription: ConvertPageInteractor
) : BaseViewModel() {

    private val _currencyFactorStateFlow = MutableStateFlow(0.0)
    val currencyFactorStateFlow = _currencyFactorStateFlow.asStateFlow()

    private val _currencyDescriptionStateFlow =
        MutableStateFlow(CurrencyDescription(null, null, null, null, null, null, null))
    val currencyDescriptionStateFlow = _currencyDescriptionStateFlow.asStateFlow()

    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow = _loadingStateFlow.asStateFlow()

    fun getCurrencyFactor(baseCurrency: String, currencies: List<String>) {
        launch {
            try {
                var currencyFactor: Double
                withContext(Dispatchers.IO) {
                    currencyFactor = interactorData.getCurrencyFactor(baseCurrency, currencies)
                }
                _currencyFactorStateFlow.tryEmit(currencyFactor)
            } catch (c: CancellationException) {
                Timber.e("/*/ Error ${c.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            }
        }
    }

    fun getCurrencyDescription(currencies: List<String>) {
        launch {
            try {
                _loadingStateFlow.tryEmit(true)
                var currencyDescription: CurrencyDescription
                withContext(Dispatchers.IO) {
                    currencyDescription = interactorDescription.getCurrencyDescription(currencies)
                }
                _currencyDescriptionStateFlow.tryEmit(currencyDescription)
            } catch (c: CancellationException) {
                Timber.e("/*/ Error ${c.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }
}
