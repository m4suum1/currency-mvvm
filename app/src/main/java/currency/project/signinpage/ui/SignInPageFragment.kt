package currency.project.signinpage.ui

import android.os.Bundle
import android.view.View
import androidx.core.widget.doAfterTextChanged
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import currency.project.R
import currency.project.common.mvvm.BaseFragment
import currency.project.databinding.FragmentSignInPageBinding
import currency.project.mainpage.ui.MainPageFragment
import currency.project.registerpage.ui.RegisterPageFragment
import currency.project.utils.FirebaseException
import currency.project.utils.extensions.*
import currency.project.utils.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class SignInPageFragment : BaseFragment(R.layout.fragment_sign_in_page) {

    private val binding: FragmentSignInPageBinding by viewBinding()
    private val auth: FirebaseAuth by lazy {
        Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        if (auth.currentUser != null) {
            replaceScreen(MainPageFragment(), popCurrent = true)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            textInputEditTextPassword.doAfterTextChanged {
                textInputLayoutPassword.catchPasswordError(it.toString())
            }
            textInputEditTextLogin.doAfterTextChanged {
                textInputLayoutLogin.catchEmailError(it.toString())
            }
            buttonSignIn.setOnClickListener {
                hideKeyboardDrop()
                val email = textInputEditTextLogin.text.toString()
                val password = textInputEditTextPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()) createSnackBar(
                    myCoordinatorLayout,
                    resources.getString(R.string.all_fields_snack)
                )
                else if (email.isNotEmpty() && password.isNotEmpty()) {
                    launch {
                        signIn(auth, email, password, myCoordinatorLayout)
                    }
                }
            }
            textViewRegister.setOnClickListener {
                replaceScreen(RegisterPageFragment())
            }
        }
    }

    private suspend fun signIn(
        auth: FirebaseAuth,
        email: String,
        password: String,
        myCoordinatorLayout: View
    ) {
        var authorization: Task<AuthResult>
        withContext(Dispatchers.IO) {
            authorization = auth.signInWithEmailAndPassword(email, password)
        }
        authorization.addOnCompleteListener { task ->
            if (task.isSuccessful) replaceScreen(
                MainPageFragment(),
                popCurrent = true
            )
            else createSnackBar(
                myCoordinatorLayout,
                FirebaseException.getErrorCode(task)
            )
        }
    }
}
